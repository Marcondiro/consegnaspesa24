import java.util.UUID;

public class Ordine {
    private UUID id;
    private Cliente cliente;

    public Ordine(Cliente cliente){
        id = UUID.randomUUID();
        this.cliente = cliente;
    }

    public UUID getId() {
        return id;
    }

    public Cliente getCliente() {
        return cliente;
    }
}