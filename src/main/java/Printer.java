public class Printer {
    public static synchronized void printHeader(){
        System.out.println("_____________________________________________________________");
        System.out.print("ORDINE \t\t\t\t\t");
        System.out.print("CLIENTE \t");
        System.out.print("STATO \t\t");
        System.out.println();
    }

    public static synchronized void print (Ordine myOrdine, String stato){
        if(myOrdine == null) return;
        System.out.print("" + myOrdine.getId() + "\t");
        System.out.print(myOrdine.getCliente().getNome() + "\t");
        System.out.print(stato + "\t");
        System.out.println();
    }
}