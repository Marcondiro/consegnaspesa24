public class Main {
    private static final String[] nomiSample = {"Alice", "Bob", "Carl", "John", "Steve", "Bill", "Linus"};
    public static void main(String[] args) {
        int numClienti = 7;
        int numDipendenti = 2;
        if(args.length == 2) {
            try{
                numClienti = Integer.parseInt(args[0]);
            }catch(Exception e){}
            try{
                numDipendenti = Integer.parseInt(args[1]);
            }catch(Exception e){}
        }
        System.out.println("Supermercato aperto con " + numDipendenti + " dipendenti e "+ numClienti + " clienti collegati");
        Printer.printHeader();

        Supermercato s = new Supermercato(numDipendenti);
        new Thread(s).start();

        for(int i=0; i<numClienti; i++){
            String nome = Integer.toString(i);
            if(i < nomiSample.length) {nome = nomiSample[i];} //Assegna nomi demo
            boolean domiciliato = (i % 2 == 0); //Clienti pari domiciliati, dispari no.

            Cliente myCliente = new Cliente(s, domiciliato, nome);
            new Thread(myCliente).start();
        }
    }
}