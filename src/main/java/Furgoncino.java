import java.util.Random;
import java.util.concurrent.BlockingQueue;

public class Furgoncino implements Runnable {
    private BlockingQueue<Ordine> ordiniSupermercato;
    private String targa;
    public Furgoncino(BlockingQueue<Ordine> ordini, String targa) {
        this.ordiniSupermercato = ordini;
        this.targa = targa;
    }

    public void run() {
        consegnaSpesa();
    }

    public void consegnaSpesa() {
        Random rnd = new Random();
        while(true){
            try{
                Ordine myOrdine = ordiniSupermercato.take();
                Printer.print(myOrdine, "In consegna da " + targa);
                Thread.sleep(rnd.nextInt(10)*1000);
                boolean esitoConsegna = myOrdine.getCliente().consegna(myOrdine.getId());
                if(!esitoConsegna)
                    Printer.print(myOrdine, "Consegna fallita");
                Printer.print(myOrdine, "Consegnato da " + targa);
                Thread.sleep(rnd.nextInt(10)*1000); //Rientro
            } catch(Exception e){e.printStackTrace();}
        }
    }
}