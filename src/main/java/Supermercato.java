import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class Supermercato implements Runnable {
    private BlockingQueue<Ordine> ordiniRicevuti;
    private BlockingQueue<Ordine> ordiniDaSpedire;
    private ConcurrentMap<UUID, Ordine> ordiniDaRitirare;
    private Personale[] dipendenti;

    public Supermercato(int numDipendenti) {
        this.ordiniRicevuti = new ArrayBlockingQueue<Ordine>(50);
        this.ordiniDaSpedire = new ArrayBlockingQueue<Ordine>(50);
        this.ordiniDaRitirare = new ConcurrentHashMap<UUID, Ordine>();
        dipendenti = new Personale[numDipendenti];
        for (int i = 0; i < numDipendenti; i++)
            dipendenti[i] = new Personale(this, "Impiegato " + Integer.toString(i));
        Furgoncino furgoncino = new Furgoncino(this.ordiniDaSpedire, "AB123CD");
        new Thread(furgoncino).start();
    }

    public BlockingQueue<Ordine> getOrdiniDaSpedire() {
        return ordiniDaSpedire;
    }

    public ConcurrentMap<UUID, Ordine> getOrdiniDaRitirare() {
        return ordiniDaRitirare;
    }

    public void ordina(Ordine ordine) {
        try {
            ordiniRicevuti.put(ordine);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public Ordine ritira(UUID idOrdine) {
        return ordiniDaRitirare.remove(idOrdine);
    }

    public void run() {
        while (true) {
            Ordine myOrdine;
            try {
                myOrdine = ordiniRicevuti.take();
                preparaOrdine(myOrdine);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private synchronized void preparaOrdine(Ordine myOrdine) {
        Personale myPersonale = accettaOrdine();
        while (myPersonale == null) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            myPersonale = accettaOrdine();
        }
        myPersonale.assegnaOrdine(myOrdine);
        new Thread(myPersonale).start();
    }

    private synchronized Personale accettaOrdine() {
        for(int i = 0; i < dipendenti.length; i++)
            if(dipendenti[i].impiega())
                return dipendenti[i];
        return null;
    }
}