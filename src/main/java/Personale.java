public class Personale implements Runnable {
    private Supermercato supermercato;
    private String nome;
    private boolean disponibilita;
    private Ordine ordineAssegnato;

    public Personale(Supermercato supermercato, String nome){
        this.supermercato = supermercato;
        this.nome = nome;
        disponibilita = true;
    }

    public synchronized boolean disponibile(){
        return disponibilita;
    }

    public synchronized boolean impiega(){
        if(disponibilita){
            disponibilita = false;
            return true;
        }
        return false;
    }

    private synchronized void libera(){
        disponibilita = true;
        synchronized(supermercato){
            supermercato.notify();
        }
    }

    public void assegnaOrdine(Ordine ordineAssegnato){
        this.ordineAssegnato = ordineAssegnato;
    }

    public void run() {
        if(ordineAssegnato == null)
            return;
        preparaOrdine();
    }

    private void preparaOrdine(){
        Printer.print(ordineAssegnato, "In preparazione da " + this.getNome());
        try{
            Thread.sleep(3000);
            Printer.print(ordineAssegnato, "Preparato da " + this.getNome());
            if(ordineAssegnato.getCliente().domiciliato())
                supermercato.getOrdiniDaSpedire().put(ordineAssegnato);
            else
                supermercato.getOrdiniDaRitirare().put(ordineAssegnato.getId(), ordineAssegnato);
        } catch(Exception e){e.printStackTrace();}
        ordineAssegnato = null;
        this.libera();
    }

    public String getNome(){
        return nome;
    }
}