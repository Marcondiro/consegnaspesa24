import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

public class Cliente implements Runnable {
    private Supermercato supermercato;
    private Map<UUID, Ordine> myOrders;
    private boolean aDomicilio;
    private String nome;

    public Cliente(Supermercato supermercato, boolean aDomicilio, String nome) {
        myOrders = new HashMap<UUID, Ordine>();
        this.supermercato = supermercato;
        this.aDomicilio = aDomicilio;
        this.nome = nome;
    }

    public void run() {
        Random rnd = new Random();
        while (true) {
            try {
                Thread.sleep(rnd.nextInt(90) * 1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            ordina();
            if(!this.domiciliato())
                ritira();
        }
    }

    private void ordina() {
        Ordine myOrdine = new Ordine(this);
        myOrders.put(myOrdine.getId(), myOrdine);
        Printer.print(myOrdine, "Aggiunto");
        supermercato.ordina(myOrdine);
    }

    private void ritira() {
        myOrders.keySet().removeIf(k -> ritira(k));
    }

    private boolean ritira(UUID idOrdine){
        Ordine ordineRitirato = supermercato.ritira(idOrdine);
        Printer.print(ordineRitirato, "Ordine ritirato");
        return (ordineRitirato != null);
    }

    public boolean consegna(UUID idOrdine) {
        Ordine ordineConsegnato = myOrders.remove(idOrdine);
        return (ordineConsegnato != null);
    }

    public boolean domiciliato(){
        return aDomicilio;
    }

    public String getNome(){
        return nome;
    }
}